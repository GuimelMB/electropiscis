import time
import bluetooth
import json
def get_bt_data():
    bd_addr = "20:17:09:28:53:50"
    port = 1
    sock = bluetooth.BluetoothSocket (bluetooth.RFCOMM)
    sock.connect((bd_addr,port))
    data = ""
    while 1:
        try:
            data += sock.recv(1024)
            data_end = data.find('\n')
            if data_end != -1:
                rec = data[:data_end]
                print data
                break
                sock.close()
        except KeyboardInterrupt:
            break
    sock.close()
    return data

def create_json(data):
    data_string = json.dumps(data, ensure_ascii=True )
    decoded = json.loads(data_string)
    double = json.loads(decoded.decode('string-escape').strip('"'))
    return double

def open_json():
    with open('data.json') as f:
        data = json.load(f)
        data["tanque"]["ID"] = 05
    print data["tanque"]["Temp"]

def create_json_file(data):
    with open('/home/pi/Project/electropiscis/datos/data.json', 'w') as the_file:
        json.dumps(data, the_file)

def create_json_graph_file(data):
    with open('/home/pi/Project/electropiscis/datos/data_graph.json') as f:
        data_graph = json.load(f)
    ##print data_graph
    for ndx, member in reversed(list(enumerate(data_graph["DO"]))):
        if ndx == 0:
            continue
        data_graph["DO"][ndx] = data_graph["DO"][ndx-1]
    for ndx, member in reversed(list(enumerate(data_graph["pH"]))):
        if ndx == 0:
            continue
        data_graph["pH"][ndx] = data_graph["pH"][ndx-1]
    for ndx, member in reversed(list(enumerate(data_graph["Temp"]))):
        if ndx == 0:
            continue
        data_graph["Temp"][ndx] = data_graph["Temp"][ndx-1]
    for ndx, member in reversed(list(enumerate(data_graph["label"]))):
        if ndx == 0:
            continue
        data_graph["label"][ndx] = data_graph["label"][ndx-1]
    
    end=len(data_graph["DO"])
    data_graph["DO"][0] = data["tanque"]["DO"]
    data_graph["pH"][0] = data["tanque"]["pH"]
    data_graph["Temp"][0] = data["tanque"]["Temp"]
    data_graph["label"][0] = data["Time"]
    ##print data_graph
    with open('/home/pi/Project/electropiscis/datos/data_graph.json', 'w') as the_file:
        json.dump(data_graph, the_file)

##def open_json3():
##    with open('data3.json') as f:
##        data = json.load(f)
##        data["tanque"]["ID"] = 95
##    print data["tanque"]["Temp"]    
    
def create_json_file(data):
    with open('/home/pi/Project/electropiscis/datos/data.json', 'w') as the_file:
        json.dump(data, the_file)

def main():
    datos = get_bt_data()
    datos = create_json(datos)
    create_json_file(datos)
##    open_json3()
    create_json_graph_file(datos)


main()
