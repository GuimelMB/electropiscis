#!/usr/bin/env python
from flask import Flask, request, render_template, Markup
import datetime, json, subprocess
def time_convert(ts):
   return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
def open_json():
   with open('/home/pi/Project/electropiscis/datos/data.json') as f:
      data = json.load(f)
   return data
def open_json_graph():
   with open('/home/pi/Project/electropiscis/datos/data_graph.json') as f:
      data = json.load(f)
   #for ndx, member in enumerate(data["label"]):
    #  data["label"][ndx] = time_convert(data["label"][ndx])
   return data


app = Flask(__name__, static_url_path='/static')

@app.route('/') 
def index():
   subprocess.call("python /home/pi/Project/electropiscis/datos/bt_connect_v3.py", shell=True)
   data = open_json()
   tvalues = [data["tanque"]["Temp"]]
   oxvalues = [data["tanque"]["DO"]]
   phvalues = [data["tanque"]["pH"]]
   print phvalues[0], oxvalues[0], tvalues[0]
   return render_template('index.html', tvalues=tvalues[0], oxvalues=oxvalues[0], phvalues=phvalues[0])

@app.route('/resumen')
def resumen():
    return render_template('resumen.html')

@app.route('/graficas')
def graficas():
   data = open_json_graph()
   labels = data["label"]
   tvalues = data["Temp"]
   oxvalues = data["DO"]
   phvalues = data["pH"]
   return render_template('graficas.html', tvalues=tvalues, oxvalues=oxvalues, phvalues=phvalues, labels=labels)

@app.route('/configuracion')
def conf():
   return render_template('conf.html')

@app.route("/chart")
def chart():
    labels = ["January","February","March","April","May","June","July","August"]
    values = [10,9,8,7,6,4,7,8]
    return render_template('chart.html', values=values, labels=labels)


if __name__ == '__main__':
   app.run(host= '0.0.0.0', debug = True)
